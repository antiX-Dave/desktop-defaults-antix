��          4      L       `   %   a   X   �   j  �   1   K  c   }                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: Eadwine Rose, 2021
Language-Team: Dutch (https://www.transifex.com/anticapitalista/teams/10162/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Je moet een applicatie specificeren om te starten Het programma dat wordt opgestart ben ik zelf. \n U zult een ander programma dan mij moeten kiezen. 