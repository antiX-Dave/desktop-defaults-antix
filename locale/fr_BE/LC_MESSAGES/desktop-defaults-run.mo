��          4      L       `   %   a   X   �   {  �   2   \  Y   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: Wallon Wallon, 2021
Language-Team: French (Belgium) (https://www.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
 Vous devez spécifier une application à exécuter Le programme lancé est moi-même. \n Vous devrez choisir un autre programme que le mien. 