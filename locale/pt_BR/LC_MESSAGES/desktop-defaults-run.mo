��          4      L       `   %   a   X   �   �  �   ;   x  �   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-09 14:12+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 É necessário especificar um aplicativo para ser executado O programa que está sendo iniciado é o mesmo que este em execução. \n Você precisará selecionar um programa diferente a ser executado. 