#!/usr/bin/env python3
#Name: desktop-defaults-set
#Version: 1.7
#Depends: python, Gtk, python-xdg
#Author: Dave (david@daveserver.info)
#Purpose: Set default applications 
#License: gplv3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import gettext
import os
import sys
import subprocess
#Variables
ButtonWidth = 200
ButtonHeight =70

#ICONS = "/usr/share/icons/antiX-numix-bevel"
#ICONS = "/usr/share/icons/antiX-numix-square"
ICONS = "/usr/share/icons/antiX-papirus"
#ICONS = "/usr/share/icons/antiX-faenza"
#ICONS = "/usr/share/icons/antiX-moka"

gettext.install(domain = "desktop-defaults-set", codeset   = 'utf-8')

class Var: 
    def process_command(self, object_type):
        command = subprocess.Popen("app-select -i %s.desktop" % object_type, stdout=subprocess.PIPE, shell=True)
        (output, err) = command.communicate()
        return str(output).split("|")
    
    def read(self):
        var = Var
        var.Terminal = self.process_command("$HOME/.local/share/desktop-defaults/terminal")
        var.WebBrowser = self.process_command("$HOME/.local/share/desktop-defaults/web-browser")
        var.FileManager = self.process_command("$HOME/.local/share/desktop-defaults/file-manager")
        var.EmailClient = self.process_command("$HOME/.local/share/desktop-defaults/email")
        var.TextEditor = self.process_command("$HOME/.local/share/desktop-defaults/editor")
        var.ImageViewer = self.process_command("$HOME/.local/share/desktop-defaults/image-viewer")
        var.VideoPlayer = self.process_command("$HOME/.local/share/desktop-defaults/video-player")
        var.AudioPlayer = self.process_command("$HOME/.local/share/desktop-defaults/audio-player")
        var.Mime = self.process_command("/usr/share/applications/galternatives")
        print (var.Mime)
        
class mainWindow(Gtk.Window):
    def button_clicked(self, widget, command):
        command = subprocess.Popen("app-select -s", stdout=subprocess.PIPE, shell=True)
        (output, err) = command.communicate()
        info = str(output).split("|")
        print (info)
    
    def build_button(self,image,text,command,column,row):
        #Increase button count by one for use of mnemonic
        #self.button_loop += 1
        
        button = Gtk.Button()
        button.set_label(text)
        button_image = Gtk.Image.new_from_file(image)
        button.set_image(button_image)
        button.set_image_position(Gtk.PositionType.LEFT)
        button.set_always_show_image(True)
        button.connect("clicked", self.button_clicked, command)
        button.set_hexpand(True)
        button.set_vexpand(True)
        button.set_size_request(ButtonWidth, ButtonHeight)
        button.set_alignment(0,0)
        button.show()
        
        self.grid.attach(button, column, row, 1, 1)
    
    def make_label(self, text,column,row):
        label = Gtk.Label()
        label.set_text(_(text))
        label.set_size_request(200,10)
        self.grid.attach(label, column, row, 1, 1)
        label.show()

    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_size_request(400,100)
        self.set_border_width(10)
        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
        self.set_title(_("Set Desktop Defaults"))
        self.show()
        
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.grid.show()
        
        #Set first button number for use of mnemonic
        self.button_loop = 0
        #Build Buttons: ICON, NAME, ACTION, COLUMN, ROW
        self.make_label(_("Terminal"),1,1)
        self.build_button(Var.Terminal[5],Var.Terminal[1]+":\n"+Var.Terminal[6],"terminal",2,1)
        self.make_label(_("Web Browser"),1,2)
        self.build_button(Var.WebBrowser[5],Var.WebBrowser[1]+":\n"+Var.WebBrowser[6],"webbrowser",2,2) 
        self.make_label(_("File Manager"),1,3)
        self.build_button(Var.FileManager[5],Var.FileManager[1]+":\n"+Var.FileManager[6],"filemanager",2,3)
        self.make_label(_("Email Client"),1,4)
        self.build_button(Var.EmailClient[5],Var.EmailClient[1]+":\n"+Var.EmailClient[6],"emailclient",2,4)
        self.make_label(_("Text Editor"),1,5)
        self.build_button(Var.TextEditor[5],Var.TextEditor[1]+":\n"+Var.TextEditor[6],"texteditor",2,5)
        self.make_label(_("Image Viewer"),1,6)
        self.build_button(Var.ImageViewer[5],Var.ImageViewer[1]+":\n"+Var.ImageViewer[6],"imageviewer",2,6)
        self.make_label(_("Video Player"),1,7)
        self.build_button(Var.VideoPlayer[5],Var.VideoPlayer[1]+":\n"+Var.VideoPlayer[6],"videoplayer",2,7)
        self.make_label(_("Audio Player"),1,8)
        self.build_button(Var.AudioPlayer[5],Var.AudioPlayer[1]+":\n"+Var.AudioPlayer[6],"audioplayer",2,8)
        self.make_label(_("Mime Type / \nAlternatives Editor"),1,9)
        self.build_button(Var.Mime[5],Var.Mime[1]+":\n"+Var.Mime[6],"galternatives",2,9)
        
Var().read()
win = mainWindow()
win.connect("delete-event", Gtk.main_quit)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL) # without this, Ctrl+C from parent term is ineffectual
Gtk.main()
